var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../bin/www");

require('./tests/adduser.js');
require('./tests/getusers.js');
require('./tests/updateuser.js');
require('./tests/deleteuser.js');
require('./tests/addmultiple.js');