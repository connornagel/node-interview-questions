var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../../bin/www");
var monk = require('monk');

var url = 'http://localhost:3000';

describe('Add multiple and get Users', function(){
	it('Adds new users with user name \'test user1\' and retrieves them', function(done){
		
		var newUser = {
			'username' : 'test user1',
			'email' : 'test1@test.com',
			'fullname' : 'Jimmy Smith',
			'age' : 27,
			'location' : 'Portland',
			'gender' : 'Male'
			};
		
		request(url)
		.post('/users/adduser')
		.send(newUser)
		.expect(201)
		.end(function(err, res){
			if(err){
				console.log(JSON.stringify(res));
				throw err;
			}
			done();
		});

	});

it('Adds new users with user name \'test user2\' and retrieves them', function(done){
		
		var newUser = {
			'username' : 'test user2',
			'email' : 'test2@test.com',
			'fullname' : 'Bob Smith',
			'age' : 27,
			'location' : 'San Francisco',
			'gender' : 'Male'
			};
		
		request(url)
		.post('/users/adduser')
		.send(newUser)
		.expect(201)
		.end(function(err, res){
			if(err){
				console.log(JSON.stringify(res));
				throw err;
			}
			done();
		});

	});

it('Adds new users with user name \'test user3\' and retrieves them', function(done){
		
		var newUser = {
			'username' : 'test user3',
			'email' : 'test3@test.com',
			'fullname' : 'Chris Applegate',
			'age' : 27,
			'location' : 'Las Vegas',
			'gender' : 'Male'
			};
		
		request(url)
		.post('/users/adduser')
		.send(newUser)
		.expect(201)
		.end(function(err, res){
			if(err){
				console.log(JSON.stringify(res));
				throw err;
			}
			done();
		});

	});

	it('Gets all users', function(done){
		request(url)
		.get('/users/userlist')
		.expect(200)
		.end(function(err, res){
			if(err){
				console.log(JSON.stringify(res));
				throw err;
			}
			var users = JSON.parse(res.text);
			console.log(JSON.stringify(users));
			expect(users.length).toEqual(3);
			done();
		});
	});


	it('deletes the \'test user\' from the database', function (done) {
		var db = monk('localhost:27017/node_interview_question');
		var collection = db.get('userlist');
		collection.findOne({ 'username': 'test user1' }, function (err, doc) {
			var userId = doc._id;
			request(url)
				.delete('/users/deleteuser/' + userId)
				.expect(200)
				.end(function (err, res) {
					if (err) {
						console.log(JSON.stringify(res));
						throw err;
					}
					db.close();
					done();
				});
		});
	});

	it('deletes the \'test user\' from the database', function (done) {
		var db = monk('localhost:27017/node_interview_question');
		var collection = db.get('userlist');
		collection.findOne({ 'username': 'test user2' }, function (err, doc) {
			var userId = doc._id;
			request(url)
				.delete('/users/deleteuser/' + userId)
				.expect(200)
				.end(function (err, res) {
					if (err) {
						console.log(JSON.stringify(res));
						throw err;
					}
					db.close();
					done();
				});
		});
	});

	it('deletes the \'test user\' from the database', function (done) {
		var db = monk('localhost:27017/node_interview_question');
		var collection = db.get('userlist');
		collection.findOne({ 'username': 'test user3' }, function (err, doc) {
			var userId = doc._id;
			request(url)
				.delete('/users/deleteuser/' + userId)
				.expect(200)
				.end(function (err, res) {
					if (err) {
						console.log(JSON.stringify(res));
						throw err;
					}
					db.close();
					done();
				});
		});
	});
});