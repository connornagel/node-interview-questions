var React = require('react');

class UserInfo extends React.Component {

	render(){
		let userList = {
			backgroundColor:'#d0cbcb',
			display:'inline-block',
		};
		let header = {
			margin:'0px',
			textAlign:'center',
			backgroundColor:'#aaadbd',
			paddingTop:'5px',
			paddingBottom:'5px',
			border:'solid 2px',
			borderRadius:'3px',
			borderColor:'grey',
		};
		let tbody = {
			backgroundColor:'#aaaaaa',
		};
		let table = {
			padding:'5px',
		};
		return(
			<div className="userList" style={userList}>
				<h2 style={header}>User List</h2>
				<table style={table}>
					<thead><tr><th>UserName</th><th>Email</th><th>Delete?</th></tr></thead>
					<tbody style={tbody}></tbody>
				</table>
			</div>
			);
	}
}

module.exports = UserInfo;