var React = require('react');
var ReactDOM = require('react-dom');
import AddUser from './AddUser.jsx';
import UserList from './UserList.jsx';
import UserInfo from './UserInfo.jsx';

class Main extends React.Component {
	
	render(){
		let styles = {
			margin:'0',		
			textAlign:'center',
		
		};
		let header = {
			backgroundColor: 'grey',
			padding:'20px',
			borderBottom:'outset 6px',
			borderLeft:'inset 12px',
			borderRadius:'3px',
			textAlign:'center',
		};
		let userPanel = {
			display:'inline-block',
			position:'relative',
			margin:'0px',
		};
		let userList ={
			backgroundColor:'#aaadbd',
		};
		let body = {
			backgroundColor:'#d7d2da',
			margin:'0',
			height:'100%',
			width:'100%',
		};
		let wrapper = {
			margin:'0px',
			marginLeft:'20%',
			marginRight:'20%',
			position:'relative',
			height:'100%',
			backgroundColor:"#9dbcd2",
			padding:'10px',
			border:'solid 2px',
			borderTop:'none',
			borderRadius:'3px',
			borderColor:'grey',
		};

		return(<div>
			<html style={body}>
				<header style={header}>
				<h1 style={styles}>Express</h1>
				</header>
				<body style={body}>
					<div style ={wrapper}>
						<div style={userPanel}>
						<UserList style={userList}/>
						<UserInfo/>
						</div>
						<AddUser/>
					</div>
				</body>
			</html>
		</div>);
	}
}


module.exports = Main;