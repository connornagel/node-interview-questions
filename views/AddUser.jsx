var React = require('react');
//require('../stylesheets/style.css');
require('jquery');

class AddUser extends React.Component {

	render(){
	
		let add = {
			display:'inline-block',
			backgroundColor:'#d0cbcb',
			position:'relative',
			float:'right',
		};
		let header ={
			margin:'0px',
			textAlign:'center',
			backgroundColor:'#aaadbd',
			paddingTop:'5px',
			paddingBottom:'5px',
			border:'solid 2px',
			borderRadius:'3px',
			borderColor:'grey',
		};
		let fieldset ={
			border:'none',
			margin:'0px',
			position:'relative',

		};
		
		return(
			<div className="addUser" style={add}>
				<h2 style={header}>Add User</h2>
					<fieldset style={fieldset}>
						<input id='inputUserName' type='text' placeholder='Username'/><br/>
						<input id='inputUserEmail' type='text' placeholder='Email'/><br/>
						<input id='inputUserFullname' type='text' placeholder='Full Name'/><br/>
						<input id='inputUserAge' type='text' placeholder='Age'/><br/>
						<input id='inputUserLocation' type='text' placeholder='Location'/><br/>
						<input id='inputUserGender' type='text' placeholder='gender'/><br/>
						<button id='btnAddUser'>Add User</button>
					</fieldset>
			</div>
			);
	}
}

module.exports = AddUser;
