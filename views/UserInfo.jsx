var React = require('react');

class UserInfo extends React.Component {

	render(){
		let header ={
			margin:'0px',
			marginTop:'20px',
			textAlign:'center',
			backgroundColor:'#aaadbd',
			paddingTop:'5px',
			paddingBottom:'5px',
			border:'solid 2px',
			borderRadius:'3px',
			borderColor:'grey',
		};
		let userInfo ={
			backgroundColor:'#d0cbcb',
		};
		let paragraph ={
			margin:'0px',
			padding:'5px',
		};
		return(
			<div className="userInfo" style={userInfo}>
				<h2 style={header}>User Info</h2>
				<p style={paragraph}>
					<span id='userInfoName'><strong>Name:</strong></span><br/>
					<span id='userInfoAge'><strong>Age:</strong></span><br/>
					<span id='userInfoGender'><strong>Gender:</strong></span><br/>
					<span id='userInfoLocation'><strong>Location:</strong></span><br/>
				</p>
			</div>
			);
	}
}


module.exports = UserInfo;