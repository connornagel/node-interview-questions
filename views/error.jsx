var React = require('react');
import ReactDOM from 'react-dom';
import Main from "./Main.jsx";

function Error(props){
		return(
			<div>
			<div>{props.message}</div>
			<div>{props.error.status}</div>
			</div>
			);
	}

module.exports = Error;