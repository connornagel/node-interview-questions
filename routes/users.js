var express = require('express');
var router = express.Router();

/*
 * GET userlist.
 */
router.get('/userlist', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    collection.find({},{},function(e,docs){
       (e === null) ? res.json(docs) : res.send({msg: "Error while retrieving userlist" + e});
        
    });
});

/*
 * POST to adduser.
 */
router.post('/adduser', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    collection.insert(req.body, function(err, result){
        res.status(201);
        res.send(
            (err === null) ? { msg: "Successfully added" } : { 
                msg: "Error occurred while attempting to add user" + JSON.stringify(req.body) +  err }
        );
    });
});

/*
 * DELETE to deleteuser.
 */
router.delete('/deleteuser/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    var userToDelete = req.params.id;
    collection.remove({ '_id' : userToDelete }, function(err) {
        res.send((err === null) ? { msg: '' } : { msg:'Error when deleting user. User:' + err });
    });
});

/*
 * PUT to updateuser.
 */
router.put('/updateuser/:id', function(req, res){
    var db = req.db;
    var collection = db.get('userlist');
    var userToUpdate = req.params.id;
    collection.update({'_id' : userToUpdate}, req.body, function(err){
        res.send((err === null) ? {msg: 'User Updated' } : {msg:'Update User error, User Id: ' + userToUpdate + "Error Log:" + err});
    });
});

module.exports = router;